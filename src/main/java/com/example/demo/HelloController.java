package com.example.demo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello") // map the method to url
    public String getHelloMessage() { // invoke method by url
        return "Hello";
    }

}
